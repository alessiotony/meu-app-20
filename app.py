print("""
       _
     (* *)
    __)#(__
   ( )...( )
   || |_| ||
   () | | ()
    _(___)_
   [-]   [-]
   
   
   APP CALCULO DO INDICE DE MASSA CORPORAL
   
      """)


nome = input('Digite seu nome: ')
altura = float(input('Digite sua altura (em metros): '))
peso = float(input('Digite seu peso (em kg): '))

imc = peso/altura**2

peso0 = 18.5*(altura**2)
peso1 = 24.9*(altura**2)

print(f"O IMC de {nome} é: {imc:.1f}")

if (imc<18.5) or (imc>25):
    print(f"O peso ideal de {nome} deveria estar entre: {peso0:.1f}kg e {peso1:.1f}kg")
else:
    print('Parabéns!!! Você está com o IMC Normal')


print('Parabéns!!!')